<?php
    include "class.php";
?>
<html>
    <head>
        <title>Object Oriented PHP</title>
    </head>
    <body>
        <p>
        <?php
            $text = 'Hello World';
            echo "$text And The Universe";
            echo '<br>';
            $msg = new Message();
            echo Message:: $count;
            //echo $msg->text;
            $msg->show();    
            $msg1 = new Message("A new text");
            $msg1->show(); 
            echo Message:: $count;   
            $msg2 = new Message();
            $msg2->show(); 
            echo '<br>';
            echo Message:: $count;
            echo '<br>';
            $msg3 = new redMessage('a red message');
            $msg3->show();
            echo '<br>';
            $msg4 = new coloredMessage('A colored message');
            $msg4->color = 'yellow';
            $msg4->show('A green message'); 
        ?>
        </p>
    </body>
</html>